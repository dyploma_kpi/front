export const environment = {
    production: false,
    authApiUrl: 'http://localhost:8000/api/v1/auth',
    profileApiUrl: 'http://localhost:8001/api/v1/profile',
    notificationApiUrl: 'http://localhost:8002/api/v1/notification',
    chatApiUrl: 'http://localhost:8003/api/v1/chat',
    teamApiUrl: 'http://localhost:8004/api/v1/team'
  };