import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { ProfileModule } from '../../../modules/drone_guy/profile/profile.module';
import { Profile } from '../../../modules/drone_guy/profile/models/profile';
import { ProfileService } from '../../../modules/drone_guy/profile/profile.service';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { UpdateProfile } from '../../../modules/drone_guy/profile/models/update_profile';

@Component({
  selector: 'app-profile-drone',
  standalone: true,
  imports: [
    ProfileModule,
    CommonModule,
    ReactiveFormsModule
  ],
  templateUrl: './profile-drone.page.html',
  styleUrl: './profile-drone.page.scss'
})
export class ProfileDroneComponent implements OnInit {
  userForm!: FormGroup;
  avatar_url!: string;

  constructor(private formBuilder: FormBuilder, private profile_service: ProfileService, private router: Router) {}

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      first_name: [''],
      last_name: [''],
      country: [''],
      city: [''],
      expirience_year: [''],
      expirience_type: [''],
      expirience_desc: [''],
      graduation_type: ['']
    });
    this.profile_service.my_profile()
      .subscribe({
        next: (profile: Profile) => {
          this.avatar_url = profile.avatar_url;
          this.userForm.patchValue(profile);
        },
        error: (e) => {
          if (e.message === 'Access token not found' || e.error.detail === 'You aren`t authorized'){
            localStorage.removeItem('accessToken');
            this.router.navigate(['/login']);
          } else {
            console.error('error:', e)
          }
        }
    });
  }

  onUpdate(): void {
    this.profile_service.update_profile(this.userForm.value as Partial<UpdateProfile>)
      .subscribe({
        next: (profile: Profile) => {
          this.avatar_url = profile.avatar_url;
          this.userForm.patchValue(profile);
          console.log("Profile updated");
        },
        error: (e) => {
          if (e.message === 'Access token not found' || e.error.detail === 'You aren`t authorized'){
            localStorage.removeItem('accessToken');
            this.router.navigate(['/login']);
          } else {
            console.error('Update profile error:', e)
          }
        }
    });
  }

}
