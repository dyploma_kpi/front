import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment.dev';
import { Token } from './models/token';
import { LoginUser } from './models/login_user';
import { RegisterUser } from './models/register_user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _url: string = environment.authApiUrl;
  constructor(private http: HttpClient) {}

  login(user: LoginUser): Observable<Token> {
    return this.http.post<Token>(`${this._url}/login`, user);
  }

  register(user: RegisterUser): Observable<any> {
    return this.http.post<any>(`${this._url}/register`, user);
  }

}
