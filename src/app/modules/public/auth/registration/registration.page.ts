import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { AuthService } from '../auth-service.service';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { RegisterUser } from '../models/register_user';

@Component({
  selector: 'app-registration',
  standalone: true,
  imports: [MatIconModule, ReactiveFormsModule, CommonModule],
  templateUrl: './registration.page.html',
  styleUrl: './registration.page.scss'
})
export class RegistrationComponent {
  registrationForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    if (localStorage.getItem('accessToken')) {
      this.router.navigate(['/drone']);
    }
    this.registrationForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.pattern(/^\+38\d{10}$/)]],
      fname: ['', [Validators.required, Validators.minLength(3)]],
      sname: ['', [Validators.required, Validators.minLength(3)]],
      password: ['', [Validators.required, Validators.minLength(12)]],
      confirm_password: ['', Validators.required]
    });// }, {
    //   validators: this.passwordMatchValidator(this.registrationForm)
    // });
  }

  passwordMatchValidator(formGroup: FormGroup) {
    const password = formGroup.get('password')?.value;
    const confirm_password = formGroup.get('confirm_password')?.value;

    return password === confirm_password ? null : { password_mismatch: true };
  }

  onSubmit(): void {
    console.log("here");
    this.authService.register(this.registrationForm.value as RegisterUser)
      .subscribe({
        next: (responce) => {
          console.log(responce.status);
          console.log("User Registared");
          this.router.navigate(['/login']);
        },
        error: (e) => console.error('Register error:', e)
    });
  }
}
