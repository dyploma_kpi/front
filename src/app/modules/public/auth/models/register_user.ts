export interface RegisterUser {
    email: string;
    phone: string;
    fname: string;
    sname: string;
    password: string;
}
