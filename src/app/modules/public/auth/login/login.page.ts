import { Component, OnInit } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AuthService } from '../auth-service.service';
import { Router } from '@angular/router';
import { LoginUser } from '../models/login_user';
import { Token } from '../models/token';


@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    MatIconModule,
    ReactiveFormsModule,
    CommonModule
  ],
  templateUrl: './login.page.html',
  styleUrl: './login.page.scss'
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    if (localStorage.getItem('accessToken')) {
      this.router.navigate(['/drone']);
    }
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(12)]]
    });
  }

  onSubmit(): void {
    this.authService.login(this.loginForm.value as LoginUser)
      .subscribe({
        next: (token: Token) => {
          localStorage.setItem('accessToken', token.token_type + " " + token.access_token);

          this.router.navigate(['/drone']);

        },
        error: (e) => console.error('Login error:', e)
    });
  }
}
