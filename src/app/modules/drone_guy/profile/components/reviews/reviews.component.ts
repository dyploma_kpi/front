import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { Review } from '../../models/review';
import { ProfileService } from '../../profile.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reviews',
  standalone: true,
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss'],
  imports: [
    CommonModule,
    MatIconModule
],
})
export class ReviewsComponent implements OnInit {
  reviews!: Review[];
  constructor(private profile_service: ProfileService, private router: Router) { }

  ngOnInit() {
    this.profile_service.get_my_reviews()
    .subscribe({
      next: (reviews: Review[]) => {
        this.reviews = reviews;
      },
      error: (e) => {
        if (e.message === 'Access token not found' || e.error.detail === 'You aren`t authorized'){
          localStorage.removeItem('accessToken');
          this.router.navigate(['/login']);
        } else {
          console.error('error:', e)
        }
      }
    });
  }

  getStarsArray(rate: number): number[] {
    return Array(rate).fill(0).map((x, i) => i + 1);
  }

  onClick(id_author: number): void {
    this.router.navigate([`/drone/profile/${id_author}`]);
  }

}
