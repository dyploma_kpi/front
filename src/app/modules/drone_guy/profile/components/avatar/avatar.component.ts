import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { Observable, map } from 'rxjs';
import { ImageCropperComponent } from '../image-cropper/image-cropper.component';
import { MatIconModule } from '@angular/material/icon';
import { ProfileService } from '../../profile.service';

@Component({
  selector: 'app-avatar',
  standalone: true,
  imports: [ ImageCropperComponent,
            CommonModule,
            MatIconModule
   ],
  templateUrl: './avatar.component.html',
  styleUrl: './avatar.component.scss'
})
export class AvatarComponent implements OnInit, ControlValueAccessor {
  @Input() avatar_url!: string | undefined;

  constructor(public dialog: MatDialog, private profile_service: ProfileService) {}

  ngOnInit(): void {}

  writeValue(_file: string): void {
    this.avatar_url = _file;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange = (fileUrl: string) => {
  };

  onTouched = () => {};

  disabled: boolean = false;

  onFileChange(event: any) {
    const files = event.target.files as FileList;

    if (files.length > 0) {
      const _file = URL.createObjectURL(files[0]);
      
      this.resetInput();
      this.openAvatarEditor(_file)
      .subscribe(
        (result) => {
          if(result){
            this.avatar_url = result;
            this.profile_service.upload_avatar(result).subscribe();
            this.onChange(result);
          }
        }
      )
      
    }
  }

  openAvatarEditor(image: string): Observable<any> {
    const dialogRef = this.dialog.open(ImageCropperComponent, {
      maxWidth: '80vw',
      maxHeight: '80vh',
      data: image,
    });

    return dialogRef.afterClosed();
  }

  resetInput(){
    const input = document.getElementById('avatar-input-file') as HTMLInputElement;
    if(input){
      input.value = "";
    }
  }
}