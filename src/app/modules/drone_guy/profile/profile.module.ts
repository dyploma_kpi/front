import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvatarComponent } from './components/avatar/avatar.component';
import { ImageCropperComponent } from './components/image-cropper/image-cropper.component';
import { FilesComponent } from './components/files/files.component';
import { ReviewsComponent } from './components/reviews/reviews.component';



@NgModule({
  exports: [AvatarComponent, FilesComponent, ReviewsComponent],
  imports: [
    CommonModule,
    AvatarComponent,
    ImageCropperComponent,
    FilesComponent,
    ReviewsComponent
  ]
})
export class ProfileModule { }
