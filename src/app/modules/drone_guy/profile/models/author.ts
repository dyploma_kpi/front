export interface Author {
    id: number;
    first_name: string;
    avatar_url: string;
}
