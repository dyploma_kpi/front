import { Author } from "./author";

export interface Review {
    rate: number;
    desc: string;
    date: Date;
    author: Author;
}