export interface UpdateProfile {
    first_name: string;
    last_name: string;
    country?: string;
    city?: string;
    expirience_year?: number;
    expirience_type?: string;
    expirience_desc?: string;
    graduation_type?: string;
}