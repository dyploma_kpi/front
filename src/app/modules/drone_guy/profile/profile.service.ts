import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, map, throwError } from 'rxjs';
import { environment } from '../../../../environments/environment.dev';
import { Profile } from './models/profile';
import { UpdateProfile } from './models/update_profile';
import { Review } from './models/review';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private profileApiUrl: string = environment.profileApiUrl;
  constructor(private http: HttpClient) {}

    my_profile(): Observable<Profile> {
        const token = localStorage.getItem('accessToken');
        if (token == null) {
            return throwError(() => new Error('Access token not found'))
        }
        const headers = new HttpHeaders({
            'Auth': token
        });

        return this.http.get<Profile>(`${this.profileApiUrl}/my`, { headers });
    }

    update_profile(profile: Partial<UpdateProfile>): Observable<Profile> {
        const token = localStorage.getItem('accessToken');
        if (token == null) {
            return throwError(() => new Error('Access token not found'))
        }
        const headers = new HttpHeaders({
            'Auth': token
        });

        return this.http.patch<Profile>(`${this.profileApiUrl}`, profile, { headers });
    }

    get_my_reviews(): Observable<Review[]> {
        const token = localStorage.getItem('accessToken');
        if (token == null) {
            return throwError(() => new Error('Access token not found'))
        }
        const headers = new HttpHeaders({
            'Auth': token
        });

        return this.http.get<Review[]>(`${this.profileApiUrl}/review/my`, { headers });
    }

    upload_avatar(fileUrl: string): Observable<void> {
        const token = localStorage.getItem('accessToken');
        if (token == null) {return new Observable<void>}
        else {
            fetch(fileUrl)
                .then(res => res.blob())
                .then(res => {
                    const formData = new FormData();
                    formData.append('file', res);
                    const headers = new HttpHeaders({
                            'Auth': token
                    });
                    this.http.post<any>(`${this.profileApiUrl}/file/avatar`, formData, { headers }).subscribe()
                })
            return new Observable<void>;

        }


      }

}
