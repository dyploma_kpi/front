import { Routes } from '@angular/router';
import { LoginComponent } from './modules/public/auth/login/login.page';
import { ForgotComponent } from './modules/public/auth/forgot/forgot.page';
import { RegistrationComponent } from './modules/public/auth/registration/registration.page';
import { DroneGuyRoutingModule } from './routes/drone_guy/drone-guy-routing.module';
import { DroneGuyComponent } from './routes/drone_guy/drone-guy.component';
import { authGuard } from './routes/auth.guard';

export const routes: Routes = [
      {
        path: 'login', component: LoginComponent,
      },
      {
        path: 'forgot', component: ForgotComponent,
      },
      {
        path: 'register', component: RegistrationComponent,
      },
      {
        path: 'drone',
        component: DroneGuyComponent,
        loadChildren: () => DroneGuyRoutingModule,
        canActivate: [authGuard],
      },
      {
        path: '**',
        redirectTo: 'login',
      },
];
